import { createStore } from "vuex";
import axios from "axios";


export default createStore({
  state: {
    dataCovid: {
      updated: "",
      country: "",
      cases: "",
      todayCases: "",
      deaths: "",
      todayDeaths: "",
      recovered: "",
      todayRecovered: "",
      active: "",
      critical: "",
      casesPerOneMillion: "",
      deathsPerOneMillion: "",
      tests: "",
      testsPerOneMillion: "",
      population: "",
      continent: "",
      oneCasePerPeople: "",
      oneDeathPerPeople: "",
      oneTestPerPeople: "",
      activePerOneMillion: "",
      recoveredPerOneMillion: "",
      criticalPerOneMillion: "",
      Confirmed: "",
      Recovered: "",
      Hospitalized: "",
      Deaths: "",
      NewConfirmed: "",
      NewRecovered: "",
      NewHospitalized: "",
      NewDeaths: "",
      UpdateDate: "",
      DevBy: "",
    },
  },

  getters: {
    todayCases(state) {
      return state.dataCovid.todayCases;
    },

    allCases(state) {
      return state.dataCovid.cases;
    },
    todayRecovered(state) {
      return state.dataCovid.todayRecovered;
    },

    allRecovered(state) {
      return state.dataCovid.recovered;
    },

    todayDeaths(state) {
      return state.dataCovid.todayDeaths;
    },

    allDeaths(state) {
      return state.dataCovid.deaths;
    },

    UpdateDate(state) {
      return state.dataCovid.UpdateDate;
    },
  },

  mutations: {
    UPDATE_TODAY_CASES(state, response) {
      state.dataCovid.todayCases = response;
      console.log(`${"Mutations: "}${response}`);
    },

    UPDATE_CASES(state, response) {
      state.dataCovid.cases = response;
      console.log(`${"Mutations: "}${response}`);
    },

    UPDATE_TODAY_REC(state, response) {
      state.dataCovid.todayRecovered = response;
      console.log(`${"Mutations REC: "}${response}`);
    },

    UPDATE_REC(state, response) {
      state.dataCovid.recovered = response;
      console.log(`${"Mutations REC: "}${response}`);
    },

    UPDATE_TODAY_DEATH(state, response) {
      state.dataCovid.todayDeaths = response;
      console.log(`${"Mutations REC: "}${response}`);
    },

    UPDATE_DEATH(state, response) {
      state.dataCovid.deaths = response;
      console.log(`${"Mutations REC: "}${response}`);
    },
    
    UPDATE_DATE(state, response) {
      state.dataCovid.UpdateDate = response;
      console.log(`${"Mutations REC: "}${response}`);
    },
  },
  actions: {
    GET_DATA_COVID(context) {
      axios.get("https://static.easysunday.com/covid-19/getTodayCases.json")
        .then((response) => {
          context.commit("UPDATE_TODAY_CASES", response.data.todayCases);
          console.log(`${"Get: "}${response.data.todayCases}`);

          context.commit("UPDATE_CASES", response.data.cases);
          console.log(`${"Get: "}${response.data.cases}`);

          context.commit("UPDATE_TODAY_REC", response.data.todayRecovered);
          console.log(`${"Get2: "}${response.data.todayRecovered}`);

          context.commit("UPDATE_REC", response.data.recovered);
          console.log(`${"Get2: "}${response.data.recovered}`);

          context.commit("UPDATE_TODAY_DEATH", response.data.todayDeaths);
          console.log(`${"Get3: "}${response.data.todayDeaths}`);

          context.commit("UPDATE_DEATH", response.data.deaths);
          console.log(`${"Get3: "}${response.data.deaths}`);

          context.commit("UPDATE_DATE", response.data.UpdateDate);
          console.log(`${"Get3: "}${response.data.UpdateDate}`);
        });
    },
  },
  modules: {},
});
